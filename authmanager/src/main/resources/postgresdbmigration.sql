CREATE TABLE IF NOT EXISTS login_methods (
    login_method_id VARCHAR(50) PRIMARY KEY,
    name VARCHAR(20),
    login_method_type VARCHAR(20),
    client_id VARCHAR(100),
    client_secret VARCHAR(100),
    app_token_redirect_uri VARCHAR(300),
    created_at BIGINT,
    last_updated_at BIGINT,
    is_active BOOLEAN
);

CREATE TABLE IF NOT EXISTS users (
    user_id VARCHAR(50) PRIMARY KEY,
    principal_name VARCHAR(100),
    created_at BIGINT,
    last_updated_at BIGINT,
    is_active BOOLEAN,

    CONSTRAINT idx_users_username UNIQUE (principal_name)
);

CREATE TABLE IF NOT EXISTS resource_schema_items (
    resource_schema_item_name VARCHAR(50) PRIMARY KEY,
    parent_resource_schema_item_name VARCHAR(50),
    available_actions  VARCHAR(50)[],

    FOREIGN KEY (parent_resource_schema_item_name) REFERENCES resource_schema_items(resource_schema_item_name) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS roles (
    role_name VARCHAR(100) PRIMARY KEY,
    is_active BOOLEAN
);

CREATE TABLE IF NOT EXISTS user_roles (
    user_role_id VARCHAR(50) PRIMARY KEY,
    role_name VARCHAR(100) REFERENCES roles(role_name) ON DELETE CASCADE,
    principal_name VARCHAR(100) REFERENCES users(principal_name) ON DELETE CASCADE,
    is_active BOOLEAN

    -- INDEX idx_user_roles_principal_name (principal_name)
);

CREATE INDEX IF NOT EXISTS idx_user_roles_principal_name_exist ON user_roles(principal_name, is_active);
CREATE UNIQUE INDEX IF NOT EXISTS idx_user_roles_role_name_principal_name ON user_roles (role_name, principal_name);

CREATE TABLE IF NOT EXISTS role_policies (
    role_policy_name VARCHAR(100) PRIMARY KEY,
    role_name VARCHAR(100) REFERENCES roles(role_name) ON DELETE CASCADE,
    granted_actions VARCHAR(50)[],
    granted_resources VARCHAR(300)[],
    effect VARCHAR(10),
    resource_schema_item_name VARCHAR(50) REFERENCES resource_schema_items(resource_schema_item_name) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS idx_role_policies_role_name ON role_policies(role_name);
