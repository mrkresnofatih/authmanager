package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import jakarta.validation.constraints.NotNull;

import java.util.List;

public class RoleDbListResponse {
    private Integer page;

    private Integer pageSize;

    private List<RoleDbGetResponse> roles;

    public RoleDbListResponse() {
    }

    public RoleDbListResponse(Integer page, Integer pageSize, List<RoleDbGetResponse> roles) {
        this.page = page;
        this.pageSize = pageSize;
        this.roles = roles;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<RoleDbGetResponse> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDbGetResponse> roles) {
        this.roles = roles;
    }
}
