package mrkresnofatih.com.gitlab.authmanager.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class PostgresConnectionConfig {
    @Value("${authmanager.postgres.connectionUrl}")
    private String postgresConnectionUrl;

    @Bean
    public Connection getPostgresConnection() throws SQLException {
        return DriverManager.getConnection(postgresConnectionUrl);
    }

    @Bean
    public PostgresDbMigrationConfig getPostgresDbMigrationConfig() {
        return new PostgresDbMigrationConfig();
    }
}
