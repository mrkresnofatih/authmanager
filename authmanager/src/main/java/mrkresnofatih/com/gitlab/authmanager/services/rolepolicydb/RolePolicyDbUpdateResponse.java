package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

public class RolePolicyDbUpdateResponse {
    private String message;

    public RolePolicyDbUpdateResponse() {
    }

    public RolePolicyDbUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
