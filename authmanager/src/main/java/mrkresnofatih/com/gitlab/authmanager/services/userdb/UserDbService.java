package mrkresnofatih.com.gitlab.authmanager.services.userdb;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface UserDbService {
    FuncResponse<UserDbCreateResponse> create(FuncRequest<UserDbCreateRequest> createRequest);
    FuncResponse<UserDbGetResponse> get(FuncRequest<UserDbGetRequest> getRequest);
    FuncResponse<UserDbListResponse> list(FuncRequest<UserDbListRequest> listRequest);
    FuncResponse<UserDbUpdateResponse> update(FuncRequest<UserDbUpdateRequest> updateRequest);
    FuncResponse<UserDbDeleteResponse> delete(FuncRequest<UserDbDeleteRequest> deleteRequest);
}
