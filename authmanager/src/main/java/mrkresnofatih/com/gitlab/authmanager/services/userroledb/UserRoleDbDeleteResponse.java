package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

public class UserRoleDbDeleteResponse {
    private String message;

    public UserRoleDbDeleteResponse() {
    }

    public UserRoleDbDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
