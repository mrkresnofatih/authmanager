package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public class RolePolicyDbCreateRequest {
    @NotNull
    @NotBlank
    private String roleName;

    @NotNull
    @NotBlank
    private String rolePolicyName;

    @NotNull
    private List<String> grantedActions;

    @NotNull
    private List<String> grantedResources;

    @NotNull
    @NotBlank
    private String resourceSchemaItemName;

    @NotNull
    @NotBlank
    private String effect;

    public RolePolicyDbCreateRequest() {
    }

    public RolePolicyDbCreateRequest(String roleName, String rolePolicyName, List<String> grantedActions, List<String> grantedResources, String resourceSchemaItemName, String effect) {
        this.roleName = roleName;
        this.rolePolicyName = rolePolicyName;
        this.grantedActions = grantedActions;
        this.grantedResources = grantedResources;
        this.resourceSchemaItemName = resourceSchemaItemName;
        this.effect = effect;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRolePolicyName() {
        return rolePolicyName;
    }

    public void setRolePolicyName(String rolePolicyName) {
        this.rolePolicyName = rolePolicyName;
    }

    public List<String> getGrantedActions() {
        return grantedActions;
    }

    public void setGrantedActions(List<String> grantedActions) {
        this.grantedActions = grantedActions;
    }

    public List<String> getGrantedResources() {
        return grantedResources;
    }

    public void setGrantedResources(List<String> grantedResources) {
        this.grantedResources = grantedResources;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }
}
