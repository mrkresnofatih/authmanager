package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.userroledb.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user-role")
public class UserRoleController {
    private final UserRoleDbService userRoleDbService;

    @Autowired
    public UserRoleController(UserRoleDbService userRoleDbService) {
        this.userRoleDbService = userRoleDbService;
    }

    @PostMapping("/create")
    public FuncResponse<UserRoleDbCreateResponse> create(@Valid @RequestBody FuncRequest<UserRoleDbCreateRequest> createRequest) {
        return userRoleDbService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<UserRoleDbGetResponse> get(@Valid @RequestBody FuncRequest<UserRoleDbGetRequest> getRequest) {
        return userRoleDbService.get(getRequest);
    }

    @PostMapping("/list")
    public FuncResponse<UserRoleDbListResponse> list(@Valid @RequestBody FuncRequest<UserRoleDbListRequest> listRequest) {
        return userRoleDbService.list(listRequest);
    }

    @PostMapping("/update")
    public FuncResponse<UserRoleDbUpdateResponse> update(@Valid @RequestBody FuncRequest<UserRoleDbUpdateRequest> updateRequest) {
        return userRoleDbService.update(updateRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<UserRoleDbDeleteResponse> delete(@Valid @RequestBody FuncRequest<UserRoleDbDeleteRequest> deleteRequest) {
        return userRoleDbService.delete(deleteRequest);
    }
}
