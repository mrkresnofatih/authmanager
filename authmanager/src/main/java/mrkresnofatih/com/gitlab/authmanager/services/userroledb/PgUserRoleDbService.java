package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Service
public class PgUserRoleDbService implements UserRoleDbService {
    private final Connection postgresConnection;
    private final Logger logger;

    @Autowired
    public PgUserRoleDbService(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgUserRoleDbService.class);
    }

    @Override
    public FuncResponse<UserRoleDbCreateResponse> create(FuncRequest<UserRoleDbCreateRequest> createRequest) {
        logger.info("start create user_role w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO user_roles (user_role_id, role_name, principal_name, is_active) " +
                    "VALUES ('%s', '%s', '%s', %s);",
                    createRequest.getData().getUserRoleId(),
                    createRequest.getData().getRoleName(),
                    createRequest.getData().getPrincipalName(),
                    createRequest.getData().getActive().toString()));
            statement.close();
            return new FuncResponse<>(new UserRoleDbCreateResponse("user_role created"));
        } catch (SQLException e) {
            logger.error("failed to create user_role, corr-id: {} & cause: {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to create user_role");
        }
    }

    @Override
    public FuncResponse<UserRoleDbGetResponse> get(FuncRequest<UserRoleDbGetRequest> getRequest) {
        logger.info("start get user_role w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM user_roles WHERE user_role_id = '%s' " +
                            "ORDER BY role_name LIMIT 1;",
                    getRequest.getData().getUserRoleId()));
            if (result.next()) {
                var userRoleId = result.getObject(1, String.class);
                var roleName = result.getObject(2, String.class);
                var principalName = result.getObject(3, String.class);
                var active = result.getObject(4, Boolean.class);
                return new FuncResponse<>(new UserRoleDbGetResponse(
                        roleName,
                        principalName,
                        userRoleId,
                        active
                ));
            }
            return new FuncResponse<>("user not found");
        } catch (SQLException e) {
            logger.error("failed to get user, corr-id: {} & cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get user");
        }
    }

    @Override
    public FuncResponse<UserRoleDbListResponse> list(FuncRequest<UserRoleDbListRequest> listRequest) {
        logger.info("start list user_role w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var sql = getListQueryString(listRequest);
            var result = statement.executeQuery(sql);
            var userRoles = new ArrayList<UserRoleDbGetResponse>();
            while (result.next()) {
                var userRoleId = result.getObject(1, String.class);
                var roleName = result.getObject(2, String.class);
                var principalName = result.getObject(3, String.class);
                var active = result.getObject(4, Boolean.class);
                userRoles.add(new UserRoleDbGetResponse(
                        roleName,
                        principalName,
                        userRoleId,
                        active
                ));
            }
            return new FuncResponse<>(new UserRoleDbListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    userRoles
            ));
        } catch (SQLException e) {
            logger.error("failed to list user roles, corr-id: {} & cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list user roles");
        }
    }

    private String getListQueryString(FuncRequest<UserRoleDbListRequest> listRequest) {
        var sqlStringBuilder = new StringBuilder("SELECT * FROM user_roles WHERE ");
        if (!listRequest.getData().getPrincipalName().isBlank()) {
            sqlStringBuilder.append(String.format("principal_name = '%s' AND ", listRequest.getData().getPrincipalName()));
        }
        if (!listRequest.getData().getRoleName().isBlank()) {
            sqlStringBuilder.append(String.format("role_name = '%s' AND ", listRequest.getData().getRoleName()));
        }
        sqlStringBuilder.append(String.format("is_active = %s ", listRequest.getData().getActive().toString()));
        sqlStringBuilder.append(String.format("ORDER BY role_name LIMIT %d OFFSET %d;",
                listRequest.getData().getPageSize(),
                listRequest.getData().getPageSize() * (listRequest.getData().getPage() - 1)));
        return sqlStringBuilder.toString();
    }

    @Override
    public FuncResponse<UserRoleDbUpdateResponse> update(FuncRequest<UserRoleDbUpdateRequest> updateRequest) {
        logger.info("start update user_role w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("UPDATE user_roles SET " +
                    "role_name = '%s', " +
                    "principal_name = '%s', " +
                    "is_active = %s WHERE user_role_id = '%s';",
                    updateRequest.getData().getRoleName(),
                    updateRequest.getData().getPrincipalName(),
                    updateRequest.getData().getActive().toString(),
                    updateRequest.getData().getUserRoleId()));
            statement.close();
            return new FuncResponse<>(new UserRoleDbUpdateResponse("user role updated"));
        } catch (SQLException e) {
            logger.error("failed to delete user_role, corr-id: {} & cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete user_role");
        }
    }

    @Override
    public FuncResponse<UserRoleDbDeleteResponse> delete(FuncRequest<UserRoleDbDeleteRequest> deleteRequest) {
        logger.info("start delete user_role w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM user_roles WHERE user_role_id = '%s';",
                    deleteRequest.getData().getUserRoleId()));
            statement.close();
            return new FuncResponse<>(new UserRoleDbDeleteResponse("user role deleted"));
        } catch (SQLException e) {
            logger.error("failed to delete user_role, corr-id: {} & cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete user_role");
        }
    }
}
