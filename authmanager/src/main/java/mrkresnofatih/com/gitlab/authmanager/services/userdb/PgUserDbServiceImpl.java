package mrkresnofatih.com.gitlab.authmanager.services.userdb;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Service
public class PgUserDbServiceImpl implements UserDbService {
    private final Connection postgresConnection;
    private final Logger logger;

    public PgUserDbServiceImpl(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgUserDbServiceImpl.class);
    }

    @Override
    public FuncResponse<UserDbCreateResponse> create(FuncRequest<UserDbCreateRequest> createRequest) {
        logger.info("start create user w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO users (user_id, principal_name, created_at, last_updated_at, is_active) " +
                    "VALUES ('%s', '%s', %d, %d, %s);",
                    createRequest.getData().getUserId(),
                    createRequest.getData().getPrincipalName(),
                    createRequest.getData().getCreatedAt(),
                    createRequest.getData().getLastUpdatedAt(),
                    createRequest.getData().getIsActive()
            ));
            statement.close();
            return new FuncResponse<>(new UserDbCreateResponse("create user success"));
        } catch (SQLException e) {
            logger.error("fail to insert user in db w. corr-id: {} & cause: {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to insert user into db");
        }
    }

    @Override
    public FuncResponse<UserDbGetResponse> get(FuncRequest<UserDbGetRequest> getRequest) {
        logger.info("start get user w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM users WHERE principal_name = '%s' LIMIT 1;",
                    getRequest.getData().getPrincipalName()));
            if (result.next()) {
                var userId = result.getObject(1, String.class);
                var principalName = result.getObject(2, String.class);
                var createdAt = result.getObject(3, Long.class);
                var lastUpdatedAt = result.getObject(4, Long.class);
                var isActive = result.getObject(5, Boolean.class);
                return new FuncResponse<>(new UserDbGetResponse(
                        userId,
                        principalName,
                        createdAt,
                        lastUpdatedAt,
                        isActive
                ));
            }
            return new FuncResponse<>("user w. provided principal_name not found");
        } catch (SQLException e) {
            logger.error("failed to get user from db w. corr-id: {} & cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get user");
        }
    }

    @Override
    public FuncResponse<UserDbListResponse> list(FuncRequest<UserDbListRequest> listRequest) {
        logger.info("start list users w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var query = getListQueryStringBuilder(listRequest).toString();
            var result = statement.executeQuery(query);
            var userList = new ArrayList<UserDbGetResponse>();
            while (result.next()) {
                var userId = result.getObject(1, String.class);
                var principalName = result.getObject(2, String.class);
                var createdAt = result.getObject(3, Long.class);
                var lastUpdatedAt = result.getObject(4, Long.class);
                var isActive = result.getObject(5, Boolean.class);
                userList.add(new UserDbGetResponse(
                        userId,
                        principalName,
                        createdAt,
                        lastUpdatedAt,
                        isActive
                ));
            }
            return new FuncResponse<>(new UserDbListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    userList
            ));
        } catch (SQLException e) {
            logger.error("failed to list users w. corr-id: {} and cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list users");
        }
    }

    private static StringBuilder getListQueryStringBuilder(FuncRequest<UserDbListRequest> listRequest) {
        var queryBuilder = new StringBuilder("SELECT * FROM users WHERE ");
        if (!listRequest.getData().getPrincipalName().isEmpty()) {
            queryBuilder.append(String.format("principal_name = '%s' AND ", listRequest.getData().getPrincipalName()));
        }
        queryBuilder.append(String.format("is_active = %s", listRequest.getData().getIsActive().toString()));
        queryBuilder.append(String.format(" ORDER BY last_updated_at LIMIT %d OFFSET %d", listRequest.getData().getPageSize(), (listRequest.getData().getPage() - 1) * listRequest.getData().getPageSize()));
        return queryBuilder;
    }

    @Override
    public FuncResponse<UserDbUpdateResponse> update(FuncRequest<UserDbUpdateRequest> updateRequest) {
        logger.info("start update user w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("UPDATE users SET " +
                    "principal_name = '%s', " +
                    "created_at = %d, " +
                    "last_updated_at = %d, " +
                    "is_active = %s WHERE user_id = '%s';",
                    updateRequest.getData().getPrincipalName(),
                    updateRequest.getData().getCreatedAt(),
                    updateRequest.getData().getLastUpdatedAt(),
                    updateRequest.getData().getIsActive(),
                    updateRequest.getData().getUserId()));
            statement.close();
            return new FuncResponse<>(new UserDbUpdateResponse("update success"));
        } catch (SQLException e) {
            logger.error("failed to update user w. corr-id: {} & cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to update user");
        }
    }

    @Override
    public FuncResponse<UserDbDeleteResponse> delete(FuncRequest<UserDbDeleteRequest> deleteRequest) {
        logger.info("start delete user w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM users WHERE principal_name = '%s';", deleteRequest.getData().getPrincipalName()));
            statement.close();
            return new FuncResponse<>(new UserDbDeleteResponse("delete success"));
        } catch (SQLException e) {
            logger.error("failed to delete user w. corr-id: {} & cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete user");
        }
    }
}
