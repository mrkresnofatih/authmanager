package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

public class UserRoleDbCreateResponse {
    private String message;

    public UserRoleDbCreateResponse() {
    }

    public UserRoleDbCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
