package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

public class ResourceSchemaItemDeleteResponse {
    private String message;

    public ResourceSchemaItemDeleteResponse() {
    }

    public ResourceSchemaItemDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
