package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

public class ResourceSchemaItemListRequest {
    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private Integer page;

    @NotNull
    @Range(min = 1, max = 50)
    private Integer pageSize;

    public ResourceSchemaItemListRequest() {
    }

    public ResourceSchemaItemListRequest(Integer page, Integer pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
