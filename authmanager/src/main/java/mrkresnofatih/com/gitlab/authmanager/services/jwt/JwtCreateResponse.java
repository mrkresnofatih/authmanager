package mrkresnofatih.com.gitlab.authmanager.services.jwt;

public class JwtCreateResponse {
    private String jwtToken;

    public JwtCreateResponse() {
    }

    public JwtCreateResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
