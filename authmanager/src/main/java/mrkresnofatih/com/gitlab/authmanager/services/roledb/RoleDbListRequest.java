package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import jakarta.validation.constraints.NotNull;

public class RoleDbListRequest {
    @NotNull
    private Integer page;

    @NotNull
    private Integer pageSize;

    @NotNull
    private String roleName;

    @NotNull
    private Boolean active;

    public RoleDbListRequest() {
    }

    public RoleDbListRequest(Integer page, Integer pageSize, String roleName, Boolean active) {
        this.page = page;
        this.pageSize = pageSize;
        this.roleName = roleName;
        this.active = active;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
