package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface RoleDbService {
    FuncResponse<RoleDbCreateResponse> create(FuncRequest<RoleDbCreateRequest> createRequest);
    FuncResponse<RoleDbGetResponse> get(FuncRequest<RoleDbGetRequest> getRequest);
    FuncResponse<RoleDbListResponse> list(FuncRequest<RoleDbListRequest> listRequest);
    FuncResponse<RoleDbDeleteResponse> delete(FuncRequest<RoleDbDeleteRequest> deleteRequest);
    FuncResponse<RoleDbUpdateResponse> update(FuncRequest<RoleDbUpdateRequest> updateRequest);
}
