package mrkresnofatih.com.gitlab.authmanager.services.userdb;

public class UserDbCreateResponse {
    private String message;

    public UserDbCreateResponse() {
    }

    public UserDbCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
