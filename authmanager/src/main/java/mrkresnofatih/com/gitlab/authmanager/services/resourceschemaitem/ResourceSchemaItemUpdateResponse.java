package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

public class ResourceSchemaItemUpdateResponse {
    private String message;

    public ResourceSchemaItemUpdateResponse() {
    }

    public ResourceSchemaItemUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
