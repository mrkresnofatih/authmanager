package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.roledb.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController {
    private final RoleDbService roleDbService;

    @Autowired
    public RoleController(RoleDbService roleDbService) {
        this.roleDbService = roleDbService;
    }

    @PostMapping("/create")
    public FuncResponse<RoleDbCreateResponse> create(@Valid @RequestBody FuncRequest<RoleDbCreateRequest> createRequest) {
        return roleDbService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<RoleDbGetResponse> get(@Valid @RequestBody FuncRequest<RoleDbGetRequest> getRequest) {
        return roleDbService.get(getRequest);
    }

    @PostMapping("/list")
    public FuncResponse<RoleDbListResponse> list(@Valid @RequestBody FuncRequest<RoleDbListRequest> listRequest) {
        return roleDbService.list(listRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<RoleDbDeleteResponse> delete(@Valid @RequestBody FuncRequest<RoleDbDeleteRequest> deleteRequest) {
        return roleDbService.delete(deleteRequest);
    }

    @PostMapping("/update")
    public FuncResponse<RoleDbUpdateResponse> update(@Valid @RequestBody FuncRequest<RoleDbUpdateRequest> updateRequest) {
        return roleDbService.update(updateRequest);
    }
}
