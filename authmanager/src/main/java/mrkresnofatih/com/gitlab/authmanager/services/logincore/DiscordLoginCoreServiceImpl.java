package mrkresnofatih.com.gitlab.authmanager.services.logincore;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscordLoginCoreServiceImpl implements LoginCoreService {
    private final Logger logger;
    public DiscordLoginCoreServiceImpl() {
        this.logger = LoggerFactory.getLogger(DiscordLoginCoreServiceImpl.class);
    }

    @Override
    public FuncResponse<LoginCoreGetTokenResponse> getToken(FuncRequest<LoginCoreGetTokenRequest> getTokenRequest) {
        logger.info("start getToken w. data: {}", getTokenRequest);
        return null;
    }

    @Override
    public FuncResponse<LoginCoreGetProfileResponse> getProfile(FuncRequest<LoginCoreGetProfileRequest> getProfileRequest) {
        logger.info("start getProfile w. data: {}", getProfileRequest);
        return null;
    }
}
