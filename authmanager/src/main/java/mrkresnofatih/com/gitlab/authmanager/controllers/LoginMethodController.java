package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login-method")
public class LoginMethodController {
    private final LoginMethodDbService loginMethodDbService;

    @Autowired
    public LoginMethodController(LoginMethodDbService loginMethodDbService) {
        this.loginMethodDbService = loginMethodDbService;
    }

    @PostMapping("/create")
    public FuncResponse<LoginMethodDbCreateResponse> create(@Valid @RequestBody FuncRequest<LoginMethodDbCreateRequest> createRequest) {
        return loginMethodDbService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<LoginMethodDbGetResponse> get(@Valid @RequestBody FuncRequest<LoginMethodDbGetRequest> getRequest) {
        return loginMethodDbService.get(getRequest);
    }

    @PostMapping("/list")
    public FuncResponse<LoginMethodDbListResponse> list(@Valid @RequestBody FuncRequest<LoginMethodDbListRequest> listRequest) {
        return loginMethodDbService.list(listRequest);
    }

    @PostMapping("/update")
    public FuncResponse<LoginMethodDbUpdateResponse> update(@Valid @RequestBody FuncRequest<LoginMethodDbUpdateRequest> updateRequest) {
        return loginMethodDbService.update(updateRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<LoginMethodDbDeleteResponse> delete(@Valid @RequestBody FuncRequest<LoginMethodDbDeleteRequest> deleteRequest) {
        return loginMethodDbService.delete(deleteRequest);
    }
}
