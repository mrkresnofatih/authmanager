package mrkresnofatih.com.gitlab.authmanager.services.roledb;

public class RoleDbCreateResponse {
    private String message;

    public RoleDbCreateResponse() {
    }

    public RoleDbCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
