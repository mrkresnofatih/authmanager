package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;


import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

public class LoginMethodDbListRequest {
    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private Integer page;

    @NotNull
    @Range(min = 1, max = 25)
    private Integer pageSize;

    @NotNull
    private String name;

    @NotNull
    private String loginMethodType;

    @NotNull
    private Boolean isActive;

    public LoginMethodDbListRequest() {
    }

    public LoginMethodDbListRequest(Integer page, Integer pageSize, String name, String loginMethodType, Boolean isActive) {
        this.page = page;
        this.pageSize = pageSize;
        this.name = name;
        this.loginMethodType = loginMethodType;
        this.isActive = isActive;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginMethodType() {
        return loginMethodType;
    }

    public void setLoginMethodType(String loginMethodType) {
        this.loginMethodType = loginMethodType;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
