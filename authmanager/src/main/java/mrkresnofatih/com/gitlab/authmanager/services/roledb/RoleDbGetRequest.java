package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class RoleDbGetRequest {
    @NotNull
    @NotBlank
    private String roleName;

    public RoleDbGetRequest() {
    }

    public RoleDbGetRequest(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
