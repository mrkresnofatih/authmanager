package mrkresnofatih.com.gitlab.authmanager.services.jwt;

import java.util.Map;

public class JwtCreateRequest {
    private Long duration;
    private String claims;

    public JwtCreateRequest() {
    }

    public JwtCreateRequest(Long duration, String claims) {
        this.duration = duration;
        this.claims = claims;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getClaims() {
        return claims;
    }

    public void setClaims(String claims) {
        this.claims = claims;
    }
}
