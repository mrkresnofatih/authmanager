package mrkresnofatih.com.gitlab.authmanager.services.roledb;

public class RoleDbGetResponse {
    private String roleName;
    private Boolean active;

    public RoleDbGetResponse() {
    }

    public RoleDbGetResponse(String roleName, Boolean active) {
        this.roleName = roleName;
        this.active = active;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
