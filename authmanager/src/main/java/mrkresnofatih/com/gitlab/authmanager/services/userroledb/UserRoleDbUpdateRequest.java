package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class UserRoleDbUpdateRequest {
    @NotNull
    @NotBlank
    private String roleName;

    @NotNull
    @NotBlank
    private String principalName;

    @NotNull
    @NotBlank
    private String userRoleId;

    @NotNull
    private Boolean active;

    public UserRoleDbUpdateRequest() {
    }

    public UserRoleDbUpdateRequest(String roleName, String principalName, String userRoleId, Boolean active) {
        this.roleName = roleName;
        this.principalName = principalName;
        this.userRoleId = userRoleId;
        this.active = active;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
