package mrkresnofatih.com.gitlab.authmanager.services.logincore;

public class LoginCoreGetProfileRequest {
    private String accessToken;

    public LoginCoreGetProfileRequest() {
    }

    public LoginCoreGetProfileRequest(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
