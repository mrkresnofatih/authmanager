package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Service
public class PgLoginMethodDbServiceImpl implements LoginMethodDbService {
    private final Connection postgresConnection;
    private final Logger logger;

    @Autowired
    public PgLoginMethodDbServiceImpl(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgLoginMethodDbServiceImpl.class);
    }

    @Override
    public FuncResponse<LoginMethodDbCreateResponse> create(FuncRequest<LoginMethodDbCreateRequest> createRequest) {
        logger.info("start create login method w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO login_methods (login_method_id, name, login_method_type, " +
                    "client_id, client_secret, app_token_redirect_uri, created_at, last_updated_at, is_active) " +
                    "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %s)",
                    createRequest.getData().getLoginMethodId(),
                    createRequest.getData().getName(),
                    createRequest.getData().getLoginMethodType(),
                    createRequest.getData().getClientId(),
                    createRequest.getData().getClientSecret(),
                    createRequest.getData().getAppTokenRedirectUri(),
                    createRequest.getData().getCreatedAt(),
                    createRequest.getData().getLastUpdatedAt(),
                    createRequest.getData().getIsActive().toString()));
            statement.close();
            return new FuncResponse<>(new LoginMethodDbCreateResponse("create login_method success"));
        } catch (SQLException e) {
            logger.error("failed to create login-method w. corr-id: {} due to {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to create login_method");
        }
    }

    @Override
    public FuncResponse<LoginMethodDbGetResponse> get(FuncRequest<LoginMethodDbGetRequest> getRequest) {
        logger.info("start get login_method w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM login_methods WHERE login_method_id = '%s' LIMIT 1;", getRequest.getData().getLoginMethodId()));
            if (result.next()) {
                var loginMethodId = result.getObject(1, String.class);
                var name = result.getObject(2, String.class);
                var loginMethodType = result.getObject(3, String.class);
                var clientId = result.getObject(4, String.class);
                var clientSecret = result.getObject(5, String.class);
                var appTokenRedirectUri = result.getObject(6, String.class);
                var createdAt = result.getObject(7, Long.class);
                var lastUpdatedAt = result.getObject(8, Long.class);
                var isActive = result.getObject(9, Boolean.class);
                return new FuncResponse<>(new LoginMethodDbGetResponse(
                        loginMethodId,
                        name,
                        loginMethodType,
                        clientId,
                        clientSecret,
                        appTokenRedirectUri,
                        createdAt,
                        lastUpdatedAt,
                        isActive
                ));
            }
            return new FuncResponse<>("login_method not found");
        } catch (SQLException e) {
            logger.error("failed to get login_method w. corr-id: {}, cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get login_method");
        }
    }

    @Override
    public FuncResponse<LoginMethodDbListResponse> list(FuncRequest<LoginMethodDbListRequest> listRequest) {
        logger.info("start list login_methods w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var query = getListQueryStringBuilder(listRequest).toString();
            var result = statement.executeQuery(query);
            var loginMethodList = new ArrayList<LoginMethodDbGetResponse>();
            while (result.next()) {
                var loginMethodId = result.getObject(1, String.class);
                var name = result.getObject(2, String.class);
                var loginMethodType = result.getObject(3, String.class);
                var clientId = result.getObject(4, String.class);
                var clientSecret = result.getObject(5, String.class);
                var appTokenRedirectUri = result.getObject(6, String.class);
                var createdAt = result.getObject(7, Long.class);
                var lastUpdatedAt = result.getObject(8, Long.class);
                var isActive = result.getObject(9, Boolean.class);
                loginMethodList.add(new LoginMethodDbGetResponse(
                        loginMethodId,
                        name,
                        loginMethodType,
                        clientId,
                        clientSecret,
                        appTokenRedirectUri,
                        createdAt,
                        lastUpdatedAt,
                        isActive
                ));
            }
            return new FuncResponse<>(new LoginMethodDbListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    loginMethodList
            ));
        } catch (SQLException e) {
            logger.error("failed to list login_methods w. corr-id: {}, w. cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list login_methods");
        }
    }

    private static StringBuilder getListQueryStringBuilder(FuncRequest<LoginMethodDbListRequest> listRequest) {
        var queryBuilder = new StringBuilder("SELECT * FROM login_methods WHERE ");
        if (!listRequest.getData().getName().isEmpty()) {
            queryBuilder.append(String.format("name = '%s' AND ", listRequest.getData().getName()));
        }
        if (!listRequest.getData().getLoginMethodType().isEmpty()) {
            queryBuilder.append(String.format("login_method_type = '%s' AND "));
        }
        queryBuilder.append(String.format("is_active = %s ", listRequest.getData().getIsActive().toString()));
        queryBuilder.append(String.format("ORDER BY last_updated_at LIMIT %d OFFSET %d", listRequest.getData().getPageSize(), (listRequest.getData().getPage() - 1) * listRequest.getData().getPageSize()));
        return queryBuilder;
    }

    @Override
    public FuncResponse<LoginMethodDbUpdateResponse> update(FuncRequest<LoginMethodDbUpdateRequest> updateRequest) {
        logger.info("start update login_methods w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("UPDATE login_methods SET " +
                    "name = '%s', " +
                    "login_method_type = '%s', " +
                    "client_id = '%s', " +
                    "client_secret = '%s', " +
                    "app_token_redirect_uri = '%s', " +
                    "created_at = %d, " +
                    "last_updated_at = %d, " +
                    "is_active = %s WHERE login_method_id = '%s';",
                    updateRequest.getData().getName(),
                    updateRequest.getData().getLoginMethodType(),
                    updateRequest.getData().getClientId(),
                    updateRequest.getData().getClientSecret(),
                    updateRequest.getData().getAppTokenRedirectUri(),
                    updateRequest.getData().getCreatedAt(),
                    updateRequest.getData().getLastUpdatedAt(),
                    updateRequest.getData().getIsActive(),
                    updateRequest.getData().getLoginMethodId()
            ));
            statement.close();
            return new FuncResponse<>(new LoginMethodDbUpdateResponse("update success"));
        } catch (SQLException e) {
            logger.error("failed to update login_method w. corr-id: {} and cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to update db login_method");
        }
    }

    @Override
    public FuncResponse<LoginMethodDbDeleteResponse> delete(FuncRequest<LoginMethodDbDeleteRequest> deleteRequest) {
        logger.info("start delete login_methods w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM login_methods WHERE login_method_id = '%s';", deleteRequest.getData().getLoginMethodId()));
            statement.close();
            return new FuncResponse<>(new LoginMethodDbDeleteResponse("delete successful"));
        } catch (SQLException e) {
            logger.error("failed to delete login_method w. corr-id: {} and cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete login_method");
        }
    }
}
