package mrkresnofatih.com.gitlab.authmanager.tools;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileReader {
    public static StringBuilder getStringBuilder(File file) throws FileNotFoundException {
        var inputStream = new FileInputStream(file);
        StringBuilder sqlTextBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader
                (new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                sqlTextBuilder.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sqlTextBuilder;
    }
}
