package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.userdb.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserDbService userDbService;

    @Autowired
    public UserController(UserDbService userDbService) {
        this.userDbService = userDbService;
    }

    @PostMapping("/create")
    public FuncResponse<UserDbCreateResponse> create(@Valid @RequestBody FuncRequest<UserDbCreateRequest> createRequest) {
        return userDbService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<UserDbGetResponse> get(@Valid @RequestBody FuncRequest<UserDbGetRequest> getRequest) {
        return userDbService.get(getRequest);
    }

    @PostMapping("/list")
    public FuncResponse<UserDbListResponse> list(@Valid @RequestBody FuncRequest<UserDbListRequest> listRequest) {
        return userDbService.list(listRequest);
    }

    @PostMapping("/update")
    public FuncResponse<UserDbUpdateResponse> update(@Valid @RequestBody FuncRequest<UserDbUpdateRequest> updateRequest) {
        return userDbService.update(updateRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<UserDbDeleteResponse> delete(@Valid @RequestBody FuncRequest<UserDbDeleteRequest> deleteRequest) {
        return userDbService.delete(deleteRequest);
    }
}
