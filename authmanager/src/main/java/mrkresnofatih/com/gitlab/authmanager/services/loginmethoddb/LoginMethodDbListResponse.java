package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

import java.util.List;

public class LoginMethodDbListResponse {
    private Integer page;
    private Integer pageSize;
    private List<LoginMethodDbGetResponse> loginMethods;

    public LoginMethodDbListResponse() {
    }

    public LoginMethodDbListResponse(Integer page, Integer pageSize, List<LoginMethodDbGetResponse> loginMethods) {
        this.page = page;
        this.pageSize = pageSize;
        this.loginMethods = loginMethods;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<LoginMethodDbGetResponse> getLoginMethods() {
        return loginMethods;
    }

    public void setLoginMethods(List<LoginMethodDbGetResponse> loginMethods) {
        this.loginMethods = loginMethods;
    }
}
