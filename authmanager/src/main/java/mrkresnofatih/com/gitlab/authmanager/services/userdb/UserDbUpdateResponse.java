package mrkresnofatih.com.gitlab.authmanager.services.userdb;

public class UserDbUpdateResponse {
    private String message;

    public UserDbUpdateResponse() {
    }

    public UserDbUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
