package mrkresnofatih.com.gitlab.authmanager.services.logincore;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GithubLoginCoreServiceImpl implements LoginCoreService {
    private final Logger logger;

    public GithubLoginCoreServiceImpl() {
        this.logger = LoggerFactory.getLogger(GithubLoginCoreServiceImpl.class);
    }

    @Override
    public FuncResponse<LoginCoreGetTokenResponse> getToken(FuncRequest<LoginCoreGetTokenRequest> getTokenRequest) {
        logger.info("start getToken w. data: {}", getTokenRequest);
        return null;
    }

    @Override
    public FuncResponse<LoginCoreGetProfileResponse> getProfile(FuncRequest<LoginCoreGetProfileRequest> getProfileRequest) {
        logger.info("start getProfile w. data: {}", getProfileRequest);
        return null;
    }
}
