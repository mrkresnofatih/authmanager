package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class RoleDbCreateRequest {
    @NotNull
    @NotBlank
    private String roleName;

    @NotNull
    private Boolean active;

    public RoleDbCreateRequest() {
    }

    public RoleDbCreateRequest(String roleName, Boolean active) {
        this.roleName = roleName;
        this.active = active;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
