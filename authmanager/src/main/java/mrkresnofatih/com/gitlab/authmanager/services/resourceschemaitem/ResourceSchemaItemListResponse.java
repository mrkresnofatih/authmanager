package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;


import java.util.List;

public class ResourceSchemaItemListResponse {
    private Integer page;
    private Integer pageSize;
    private List<ResourceSchemaItemGetResponse> resourceSchemaItems;

    public ResourceSchemaItemListResponse() {
    }

    public ResourceSchemaItemListResponse(Integer page, Integer pageSize, List<ResourceSchemaItemGetResponse> resourceSchemaItems) {
        this.page = page;
        this.pageSize = pageSize;
        this.resourceSchemaItems = resourceSchemaItems;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<ResourceSchemaItemGetResponse> getResourceSchemaItems() {
        return resourceSchemaItems;
    }

    public void setResourceSchemaItems(List<ResourceSchemaItemGetResponse> resourceSchemaItems) {
        this.resourceSchemaItems = resourceSchemaItems;
    }
}
