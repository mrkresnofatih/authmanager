package mrkresnofatih.com.gitlab.authmanager.services.userdb;

public class UserDbDeleteResponse {
    private String message;

    public UserDbDeleteResponse() {
    }

    public UserDbDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
