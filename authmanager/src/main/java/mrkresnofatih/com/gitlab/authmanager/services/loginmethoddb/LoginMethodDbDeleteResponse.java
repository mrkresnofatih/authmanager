package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

public class LoginMethodDbDeleteResponse {
    private String message;

    public LoginMethodDbDeleteResponse() {
    }

    public LoginMethodDbDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
