package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface ResourceSchemaItemService {
    FuncResponse<ResourceSchemaItemCreateResponse> create(FuncRequest<ResourceSchemaItemCreateRequest> createRequest);
    FuncResponse<ResourceSchemaItemGetResponse> get(FuncRequest<ResourceSchemaItemGetRequest> getRequest);
    FuncResponse<ResourceSchemaItemListResponse> list(FuncRequest<ResourceSchemaItemListRequest> listRequest);
    FuncResponse<ResourceSchemaItemUpdateResponse> update(FuncRequest<ResourceSchemaItemUpdateRequest> updateRequest);
    FuncResponse<ResourceSchemaItemDeleteResponse> delete(FuncRequest<ResourceSchemaItemDeleteRequest> deleteRequest);
}
