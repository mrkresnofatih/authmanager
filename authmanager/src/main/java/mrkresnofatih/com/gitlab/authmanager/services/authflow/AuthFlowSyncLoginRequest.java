package mrkresnofatih.com.gitlab.authmanager.services.authflow;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class AuthFlowSyncLoginRequest {
    @NotNull
    @NotBlank
    private String loginMethodId;

    @NotNull
    @NotBlank
    private String authorizationCode;

    public AuthFlowSyncLoginRequest() {
    }

    public AuthFlowSyncLoginRequest(String loginMethodId, String authorizationCode) {
        this.loginMethodId = loginMethodId;
        this.authorizationCode = authorizationCode;
    }

    public String getLoginMethodId() {
        return loginMethodId;
    }

    public void setLoginMethodId(String loginMethodId) {
        this.loginMethodId = loginMethodId;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }
}
