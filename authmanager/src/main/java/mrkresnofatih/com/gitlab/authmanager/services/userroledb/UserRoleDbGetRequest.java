package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class UserRoleDbGetRequest {
    @NotNull
    @NotBlank
    private String userRoleId;

    public UserRoleDbGetRequest() {
    }

    public UserRoleDbGetRequest(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }
}
