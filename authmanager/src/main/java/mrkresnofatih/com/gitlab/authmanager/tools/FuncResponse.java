package mrkresnofatih.com.gitlab.authmanager.tools;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FuncResponse<T> {
    private T data;
    private String errorMessage;

    public FuncResponse() {
    }

    public FuncResponse(T data) {
        this.data = data;
        this.errorMessage = "";
    }

    public FuncResponse(String errorMessage) {
        this.data = null;
        this.errorMessage = errorMessage;
    }

    public FuncResponse(T data, String errorMessage) {
        this.data = data;
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonIgnore
    public boolean isError() {
        return !this.errorMessage.equals("");
    }
}