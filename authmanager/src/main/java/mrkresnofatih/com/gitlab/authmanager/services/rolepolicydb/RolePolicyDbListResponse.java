package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

import java.util.List;

public class RolePolicyDbListResponse {
    private Integer page;
    private Integer pageSize;
    private List<RolePolicyDbGetResponse> rolePolicies;

    public RolePolicyDbListResponse() {
    }

    public RolePolicyDbListResponse(Integer page, Integer pageSize, List<RolePolicyDbGetResponse> rolePolicies) {
        this.page = page;
        this.pageSize = pageSize;
        this.rolePolicies = rolePolicies;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<RolePolicyDbGetResponse> getRolePolicies() {
        return rolePolicies;
    }

    public void setRolePolicies(List<RolePolicyDbGetResponse> rolePolicies) {
        this.rolePolicies = rolePolicies;
    }
}
