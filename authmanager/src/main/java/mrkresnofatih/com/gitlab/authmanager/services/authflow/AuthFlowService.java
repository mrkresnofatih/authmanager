package mrkresnofatih.com.gitlab.authmanager.services.authflow;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface AuthFlowService {
    FuncResponse<AuthFlowSyncLoginResponse> syncLogin(FuncRequest<AuthFlowSyncLoginRequest> syncLoginRequest);
}
