package mrkresnofatih.com.gitlab.authmanager.configurations;

import mrkresnofatih.com.gitlab.authmanager.tools.FileReader;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgresDbMigrationRunner {
    public static void runDbMigration(Connection postgresConnection, PostgresDbMigrationConfig postgresDbMigrationConfig) {
        var logger = LoggerFactory.getLogger(PostgresDbMigrationRunner.class);
        if (!postgresDbMigrationConfig.getExecuted()) {
            logger.info("Running DB Migration");
            try {
                var file = ResourceUtils.getFile("classpath:postgresdbmigration.sql");
                StringBuilder sqlTextBuilder = FileReader.getStringBuilder(file);
                Statement statement = postgresConnection.createStatement();
                statement.executeUpdate(sqlTextBuilder.toString());
                statement.close();
            } catch (FileNotFoundException | SQLException e) {
                throw new RuntimeException(e);
            }
            postgresDbMigrationConfig.setExecuted(true);
        }
    }
}
