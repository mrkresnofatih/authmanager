package mrkresnofatih.com.gitlab.authmanager.services.roledb;

public class RoleDbDeleteResponse {
    private String message;

    public RoleDbDeleteResponse() {
    }

    public RoleDbDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
