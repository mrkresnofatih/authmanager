package mrkresnofatih.com.gitlab.authmanager.services.jwt;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface JwtService {
    FuncResponse<JwtCreateResponse> create(FuncRequest<JwtCreateRequest> createRequest);
}
