package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface UserRoleDbService {
    FuncResponse<UserRoleDbCreateResponse> create(FuncRequest<UserRoleDbCreateRequest> createRequest);
    FuncResponse<UserRoleDbGetResponse> get(FuncRequest<UserRoleDbGetRequest> getRequest);
    FuncResponse<UserRoleDbListResponse> list(FuncRequest<UserRoleDbListRequest> listRequest);
    FuncResponse<UserRoleDbUpdateResponse> update(FuncRequest<UserRoleDbUpdateRequest> updateRequest);
    FuncResponse<UserRoleDbDeleteResponse> delete(FuncRequest<UserRoleDbDeleteRequest> deleteRequest);
}
