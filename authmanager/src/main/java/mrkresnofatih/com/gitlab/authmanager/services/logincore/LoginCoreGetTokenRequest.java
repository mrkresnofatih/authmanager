package mrkresnofatih.com.gitlab.authmanager.services.logincore;

public class LoginCoreGetTokenRequest {
    private String authorizationCode;

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    public LoginCoreGetTokenRequest() {
    }

    public LoginCoreGetTokenRequest(String authorizationCode, String clientId, String clientSecret, String redirectUri) {
        this.authorizationCode = authorizationCode;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.redirectUri = redirectUri;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }
}
