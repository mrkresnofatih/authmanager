package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PgResourceSchemaItemServiceImpl implements ResourceSchemaItemService {
    private final Connection postgresConnection;
    private final Logger logger;

    @Autowired
    public PgResourceSchemaItemServiceImpl(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgResourceSchemaItemServiceImpl.class);
    }

    @Override
    public FuncResponse<ResourceSchemaItemCreateResponse> create(FuncRequest<ResourceSchemaItemCreateRequest> createRequest) {
        logger.info("start create resource_schema_item w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var parent = createRequest.getData().getParentResourceSchemaItemName().equals("-") ? "NULL" : String.format("'%s'",
                    createRequest.getData().getParentResourceSchemaItemName());
            var sql = String.format("INSERT INTO resource_schema_items (resource_schema_item_name, parent_resource_schema_item_name, available_actions) " +
                            "VALUES ('%s', %s, %s);",
                    createRequest.getData().getResourceSchemaItemName(),
                    parent,
                    getAvailableActionsPsqlArray(createRequest.getData().getAvailableActions()));
            logger.info("executing query: {} and corr-id: {}", sql, createRequest.getCorrelationId());
            statement.executeUpdate(sql);
            statement.close();
            return new FuncResponse<>(new ResourceSchemaItemCreateResponse("resource schema item created"));
        } catch (SQLException e) {
            logger.error("failed to create resource schema item w. corr-id: {} & cause: {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to create resource schema item");
        }
    }

    private String getAvailableActionsPsqlArray(List<String> availableActions) {
        var psqlArrayBuilder = new StringBuilder("ARRAY[");
        for(int i = 0; i < availableActions.size(); i++) {
            psqlArrayBuilder.append("'");
            psqlArrayBuilder.append(availableActions.get(i));
            psqlArrayBuilder.append("'");
            if (i != availableActions.size() - 1) {
                psqlArrayBuilder.append(", ");
            }
        }
        psqlArrayBuilder.append("]");
        return psqlArrayBuilder.toString();
    }

    @Override
    public FuncResponse<ResourceSchemaItemGetResponse> get(FuncRequest<ResourceSchemaItemGetRequest> getRequest) {
        logger.info("start get resource_schema_item w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM resource_schema_items WHERE resource_schema_item_name = '%s' LIMIT 1;",
                    getRequest.getData().getResourceSchemaItemName()));
            if (result.next()) {
                var resourceSchemaItemName = result.getObject(1, String.class);
                var parentResourceSchemaItemName = result.getObject(2, String.class);
                var availableActions = Arrays.asList((String[]) result.getArray("available_actions").getArray());
                return new FuncResponse<>(new ResourceSchemaItemGetResponse(
                        resourceSchemaItemName,
                        parentResourceSchemaItemName,
                        availableActions
                ));
            }
            return new FuncResponse<>("target resource_schema_item not found");
        } catch (SQLException e) {
            logger.error("failed to get resource_schema_item, corr-id: {} & cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get resource_schema_item");
        }
    }

    @Override
    public FuncResponse<ResourceSchemaItemListResponse> list(FuncRequest<ResourceSchemaItemListRequest> listRequest) {
        logger.info("start list resource_schema_item w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM resource_schema_items " +
                    "ORDER BY resource_schema_item_name LIMIT %d OFFSET %d;",
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize() * (listRequest.getData().getPage() - 1)));
            var resourceSchemaItemList = new ArrayList<ResourceSchemaItemGetResponse>();
            while (result.next()) {
                var resourceSchemaItemName = result.getObject(1, String.class);
                var parentResourceSchemaItemName = result.getObject(2, String.class);
                var availableActions = Arrays.asList((String[]) result.getArray("available_actions").getArray());
                resourceSchemaItemList.add(new ResourceSchemaItemGetResponse(
                        resourceSchemaItemName,
                        parentResourceSchemaItemName,
                        availableActions
                ));
            }
            return new FuncResponse<>(new ResourceSchemaItemListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    resourceSchemaItemList));
        } catch (SQLException e) {
            logger.error("Failed to list resource_schema_items w. corr-id: {} & cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list resource_schema_items");
        }
    }

    @Override
    public FuncResponse<ResourceSchemaItemUpdateResponse> update(FuncRequest<ResourceSchemaItemUpdateRequest> updateRequest) {
        logger.info("start update resource_schema_item w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var parent = updateRequest.getData().getParentResourceSchemaItemName().equals("-") ? "NULL": String.format("'%s'",
                    updateRequest.getData().getParentResourceSchemaItemName());
            statement.executeUpdate(String.format("UPDATE resource_schema_items SET " +
                    "parent_resource_schema_item_name = %s," +
                    "available_actions = %s WHERE resource_schema_item_name = '%s';",
                    parent,
                    getAvailableActionsPsqlArray(updateRequest.getData().getAvailableActions()),
                    updateRequest.getData().getResourceSchemaItemName()));
            statement.close();
            return new FuncResponse<>(new ResourceSchemaItemUpdateResponse("resource_schema_item updated successfully"));
        } catch (SQLException e) {
            logger.error("failed to update resource_schema_item, corr-id: {} & cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to update resource_schema_item");
        }
    }

    @Override
    public FuncResponse<ResourceSchemaItemDeleteResponse> delete(FuncRequest<ResourceSchemaItemDeleteRequest> deleteRequest) {
        logger.info("start delete resource_schema_item w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM resource_schema_items WHERE resource_schema_item_name = '%s';",
                    deleteRequest.getData().getResourceSchemaItemName()));
            statement.close();
            return new FuncResponse<>(new ResourceSchemaItemDeleteResponse("delete resource_schema_item success"));
        } catch (SQLException e) {
            logger.error("failed to delete resource_schema_item, corr-id: {} & cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete resource_schema_item");
        }
    }
}
