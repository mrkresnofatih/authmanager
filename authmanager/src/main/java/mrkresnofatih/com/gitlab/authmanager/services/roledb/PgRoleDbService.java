package mrkresnofatih.com.gitlab.authmanager.services.roledb;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Service
public class PgRoleDbService implements RoleDbService {
    private final Connection postgresConnection;
    private final Logger logger;

    public PgRoleDbService(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgRoleDbService.class);
    }

    @Override
    public FuncResponse<RoleDbCreateResponse> create(FuncRequest<RoleDbCreateRequest> createRequest) {
        logger.info("start create role w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO roles (role_name, is_active) " +
                    "VALUES ('%s', %s)",
                    createRequest.getData().getRoleName(),
                    createRequest.getData().getActive().toString()));
            statement.close();
            return new FuncResponse<>(new RoleDbCreateResponse("role created"));
        } catch (SQLException e) {
            logger.error("failed to create role, corr-id: {} & cause: {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to create role");
        }
    }

    @Override
    public FuncResponse<RoleDbGetResponse> get(FuncRequest<RoleDbGetRequest> getRequest) {
        logger.info("start get w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var results = statement.executeQuery(String.format("SELECT * FROM roles WHERE role_name = '%s' LIMIT 1;",
                    getRequest.getData().getRoleName()));
            if (results.next()) {
                var roleName = results.getObject(1, String.class);
                var active = results.getObject(2, Boolean.class);
                return new FuncResponse<>(new RoleDbGetResponse(
                        roleName,
                        active
                ));
            }
            return new FuncResponse<>("role not found");
        } catch (SQLException e) {
            logger.error("failed to get role, corr-id: {} & cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get role");
        }
    }

    @Override
    public FuncResponse<RoleDbListResponse> list(FuncRequest<RoleDbListRequest> listRequest) {
        logger.info("start list roles w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(getListQueryString(listRequest));
            var roles = new ArrayList<RoleDbGetResponse>();
            while (result.next()) {
                var roleName = result.getObject(1, String.class);
                var active = result.getObject(2, Boolean.class);
                roles.add(new RoleDbGetResponse(roleName, active));
            }
            return new FuncResponse<>(new RoleDbListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    roles
            ));
        } catch (SQLException e) {
            logger.error("failed to list roles, corr-id: {} & cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list roles");
        }
    }

    private String getListQueryString(FuncRequest<RoleDbListRequest> listRequest) {
        var query = new StringBuilder("SELECT * FROM roles WHERE ");
        if (!listRequest.getData().getRoleName().isBlank()) {
            query.append(String.format("role_name = '%s' AND ", listRequest.getData().getRoleName()));
        }
        query.append(String.format("is_active = %s ", listRequest.getData().getActive().toString()));
        query.append(String.format("ORDER BY role_name LIMIT %d OFFSET %d;", listRequest.getData().getPageSize(), (listRequest.getData().getPage() - 1) * listRequest.getData().getPageSize()));
        return query.toString();
    }

    @Override
    public FuncResponse<RoleDbDeleteResponse> delete(FuncRequest<RoleDbDeleteRequest> deleteRequest) {
        logger.info("start delete w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM roles WHERE role_name = '%s';",
                    deleteRequest.getData().getRoleName()));
            statement.close();
            return new FuncResponse<>(new RoleDbDeleteResponse("role deleted"));
        } catch (SQLException e) {
            logger.error("failed to delete role, corr-id: {} & cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete role");
        }
    }

    @Override
    public FuncResponse<RoleDbUpdateResponse> update(FuncRequest<RoleDbUpdateRequest> updateRequest) {
        logger.info("start update w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("UPDATE roles SET is_active = %s WHERE role_name = '%s';",
                    updateRequest.getData().getActive().toString(),
                    updateRequest.getData().getRoleName()));
            statement.close();
            return new FuncResponse<>(new RoleDbUpdateResponse("role updated"));
        } catch (SQLException e) {
            logger.error("failed to update role, corr-id: {} & cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to update role");
        }
    }
}
