package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

public class RolePolicyDbDeleteResponse {
    private String message;

    public RolePolicyDbDeleteResponse() {
    }

    public RolePolicyDbDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
