package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

public class RolePolicyDbCreateResponse {
    private String message;

    public RolePolicyDbCreateResponse() {
    }

    public RolePolicyDbCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
