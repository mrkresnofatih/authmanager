package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;


import java.util.List;

public class RolePolicyDbGetResponse {
    private String roleName;
    private String rolePolicyName;
    private List<String> grantedActions;
    private List<String> grantedResources;
    private String resourceSchemaItemName;
    private String effect;

    public RolePolicyDbGetResponse() {
    }

    public RolePolicyDbGetResponse(String roleName, String rolePolicyName, List<String> grantedActions, List<String> grantedResources, String resourceSchemaItemName, String effect) {
        this.roleName = roleName;
        this.rolePolicyName = rolePolicyName;
        this.grantedActions = grantedActions;
        this.grantedResources = grantedResources;
        this.resourceSchemaItemName = resourceSchemaItemName;
        this.effect = effect;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRolePolicyName() {
        return rolePolicyName;
    }

    public void setRolePolicyName(String rolePolicyName) {
        this.rolePolicyName = rolePolicyName;
    }

    public List<String> getGrantedActions() {
        return grantedActions;
    }

    public void setGrantedActions(List<String> grantedActions) {
        this.grantedActions = grantedActions;
    }

    public List<String> getGrantedResources() {
        return grantedResources;
    }

    public void setGrantedResources(List<String> grantedResources) {
        this.grantedResources = grantedResources;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }
}
