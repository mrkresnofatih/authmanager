package mrkresnofatih.com.gitlab.authmanager.tools;

import java.util.Base64;

public class Base64Converter {
    public static String encodeBase64(String data) {
        var encoder = Base64.getEncoder();
        return encoder.encodeToString(data.getBytes());
    }

    public static String decodeBase64(String data) {
        var decoder = Base64.getDecoder();
        var bytes = decoder.decode(data);
        return new String(bytes);
    }
}
