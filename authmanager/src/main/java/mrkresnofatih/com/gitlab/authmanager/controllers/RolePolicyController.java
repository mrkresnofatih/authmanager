package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role-policy")
public class RolePolicyController {
    private final RolePolicyDbService rolePolicyDbService;

    @Autowired
    public RolePolicyController(RolePolicyDbService rolePolicyDbService) {
        this.rolePolicyDbService = rolePolicyDbService;
    }

    @PostMapping("/create")
    public FuncResponse<RolePolicyDbCreateResponse> create(@Valid @RequestBody FuncRequest<RolePolicyDbCreateRequest> createRequest) {
        return rolePolicyDbService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<RolePolicyDbGetResponse> get(@Valid @RequestBody FuncRequest<RolePolicyDbGetRequest> getRequest) {
        return rolePolicyDbService.get(getRequest);
    }

    @PostMapping("/update")
    public FuncResponse<RolePolicyDbUpdateResponse> update(@Valid @RequestBody FuncRequest<RolePolicyDbUpdateRequest> updateRequest) {
        return rolePolicyDbService.update(updateRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<RolePolicyDbDeleteResponse> delete(@Valid @RequestBody FuncRequest<RolePolicyDbDeleteRequest> deleteRequest) {
        return rolePolicyDbService.delete(deleteRequest);
    }

    @PostMapping("/list")
    public FuncResponse<RolePolicyDbListResponse> list(@Valid @RequestBody FuncRequest<RolePolicyDbListRequest> listRequest) {
        return rolePolicyDbService.list(listRequest);
    }
}
