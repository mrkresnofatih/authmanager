package mrkresnofatih.com.gitlab.authmanager.services.userdb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class UserDbDeleteRequest {
    @NotNull
    @NotBlank
    private String principalName;

    public UserDbDeleteRequest() {
    }

    public UserDbDeleteRequest(String principalName) {
        this.principalName = principalName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }
}
