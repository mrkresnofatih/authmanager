package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

public class LoginMethodDbUpdateResponse {
    private String message;

    public LoginMethodDbUpdateResponse() {
    }

    public LoginMethodDbUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
