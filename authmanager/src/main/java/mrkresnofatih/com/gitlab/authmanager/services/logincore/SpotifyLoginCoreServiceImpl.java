package mrkresnofatih.com.gitlab.authmanager.services.logincore;

import com.fasterxml.jackson.databind.ObjectMapper;
import mrkresnofatih.com.gitlab.authmanager.tools.Base64Converter;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class SpotifyLoginCoreServiceImpl implements LoginCoreService {
    private final Logger logger;

    public SpotifyLoginCoreServiceImpl() {
        this.logger = LoggerFactory.getLogger(SpotifyLoginCoreServiceImpl.class);
    }

    @Override
    public FuncResponse<LoginCoreGetTokenResponse> getToken(FuncRequest<LoginCoreGetTokenRequest> getTokenRequest) {
        logger.info("start getToken w. data: {}", getTokenRequest);
        try {
            var base64ClientCredentials = Base64Converter
                    .encodeBase64(String.format("%s:%s", getTokenRequest.getData().getClientId(), getTokenRequest.getData().getClientSecret()));
            var httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("https://accounts.spotify.com/api/token"))
                    .header("Authorization", String.format("Basic %s", base64ClientCredentials))
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Accept", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(String.format("code=%s&redirect_uri=%s&grant_type=authorization_code", getTokenRequest.getData().getAuthorizationCode(), getTokenRequest.getData().getRedirectUri())))
                    .build();
            var httpClient = HttpClient
                    .newBuilder()
                    .connectTimeout(Duration.ofSeconds(10))
                    .build();
            var httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                var responseBody = httpResponse.body();
                logger.info("get token api response body: {}, corr-id: {}", responseBody, getTokenRequest.getCorrelationId());
                var accessToken = new ObjectMapper()
                        .readTree(responseBody)
                        .path("access_token")
                        .asText();
                return new FuncResponse<>(new LoginCoreGetTokenResponse(accessToken));
            }
            logger.error("http response status code was not 200 OK but {}, corr-id: {}", httpResponse.statusCode(), getTokenRequest.getCorrelationId());
            return new FuncResponse<>("failed to get access token");
        } catch (Exception e) {
            logger.error("failed to get access token, corr-id: {} & cause: {}", getTokenRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get access token");
        }
    }

    @Override
    public FuncResponse<LoginCoreGetProfileResponse> getProfile(FuncRequest<LoginCoreGetProfileRequest> getProfileRequest) {
        logger.info("start get profile w. data: {}", getProfileRequest);
        try {
            var httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("https://api.spotify.com/v1/me"))
                    .header("Authorization", String.format("Bearer %s", getProfileRequest.getData().getAccessToken()))
                    .header("Accept", "application/json")
                    .GET()
                    .build();
            var httpClient = HttpClient
                    .newBuilder()
                    .connectTimeout(Duration.ofSeconds(10))
                    .build();
            var httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                var responseBody = httpResponse.body();
                logger.info("get profile response: {} w. corr-id: {}", responseBody, getProfileRequest.getCorrelationId());
                var jsonNode = new ObjectMapper().readTree(responseBody);
                var email = jsonNode
                        .path("email")
                        .asText();
                if (!email.isBlank()) {
                    return new FuncResponse<>(new LoginCoreGetProfileResponse(email));
                }
                var id = jsonNode
                        .path("id")
                        .asText();
                if (!id.isBlank()) {
                    return new FuncResponse<>(new LoginCoreGetProfileResponse(id));
                }
                return new FuncResponse<>("failed to get valid keys from response for principalName");
            }
            logger.error("API get profile response is not 200 OK, corr-id: {}", getProfileRequest.getCorrelationId());
            return new FuncResponse<>("api response not 200 OK");
        } catch (Exception e) {
            logger.error("get profile failed, corr-id: {} & cause: {}", getProfileRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("get profile failed");
        }
    }
}
