package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;


import java.util.List;

public class ResourceSchemaItemGetResponse {
    private String resourceSchemaItemName;
    private String parentResourceSchemaItemName;
    private List<String> availableActions;

    public ResourceSchemaItemGetResponse() {
    }

    public ResourceSchemaItemGetResponse(String resourceSchemaItemName, String parentResourceSchemaItemName, List<String> availableActions) {
        this.resourceSchemaItemName = resourceSchemaItemName;
        this.parentResourceSchemaItemName = parentResourceSchemaItemName;
        this.availableActions = availableActions;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }

    public String getParentResourceSchemaItemName() {
        return parentResourceSchemaItemName;
    }

    public void setParentResourceSchemaItemName(String parentResourceSchemaItemName) {
        this.parentResourceSchemaItemName = parentResourceSchemaItemName;
    }

    public List<String> getAvailableActions() {
        return availableActions;
    }

    public void setAvailableActions(List<String> availableActions) {
        this.availableActions = availableActions;
    }
}
