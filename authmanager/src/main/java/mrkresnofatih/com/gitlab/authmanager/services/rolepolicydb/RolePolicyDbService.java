package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface RolePolicyDbService {
    FuncResponse<RolePolicyDbCreateResponse> create(FuncRequest<RolePolicyDbCreateRequest> createRequest);
    FuncResponse<RolePolicyDbGetResponse> get(FuncRequest<RolePolicyDbGetRequest> getRequest);
    FuncResponse<RolePolicyDbUpdateResponse> update(FuncRequest<RolePolicyDbUpdateRequest> updateRequest);
    FuncResponse<RolePolicyDbDeleteResponse> delete(FuncRequest<RolePolicyDbDeleteRequest> deleteRequest);
    FuncResponse<RolePolicyDbListResponse> list(FuncRequest<RolePolicyDbListRequest> listRequest);
}
