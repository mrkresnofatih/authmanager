package mrkresnofatih.com.gitlab.authmanager.services.userdb;

public class UserDbGetResponse {
    private String userId;
    private String principalName;
    private Long createdAt;
    private Long lastUpdatedAt;
    private Boolean isActive;

    public UserDbGetResponse() {
    }

    public UserDbGetResponse(String userId, String principalName, Long createdAt, Long lastUpdatedAt, Boolean isActive) {
        this.userId = userId;
        this.principalName = principalName;
        this.createdAt = createdAt;
        this.lastUpdatedAt = lastUpdatedAt;
        this.isActive = isActive;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Long lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
