package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;


public class LoginMethodDbGetResponse {
    private String loginMethodId;
    private String name;
    private String loginMethodType;
    private String clientId;
    private String clientSecret;
    private String appTokenRedirectUri;
    private Long createdAt;
    private Long lastUpdatedAt;
    private Boolean isActive;

    public LoginMethodDbGetResponse() {
    }

    public LoginMethodDbGetResponse(String loginMethodId, String name, String loginMethodType, String clientId, String clientSecret, String appTokenRedirectUri, Long createdAt, Long lastUpdatedAt, Boolean isActive) {
        this.loginMethodId = loginMethodId;
        this.name = name;
        this.loginMethodType = loginMethodType;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.appTokenRedirectUri = appTokenRedirectUri;
        this.createdAt = createdAt;
        this.lastUpdatedAt = lastUpdatedAt;
        this.isActive = isActive;
    }

    public String getLoginMethodId() {
        return loginMethodId;
    }

    public void setLoginMethodId(String loginMethodId) {
        this.loginMethodId = loginMethodId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginMethodType() {
        return loginMethodType;
    }

    public void setLoginMethodType(String loginMethodType) {
        this.loginMethodType = loginMethodType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getAppTokenRedirectUri() {
        return appTokenRedirectUri;
    }

    public void setAppTokenRedirectUri(String appTokenRedirectUri) {
        this.appTokenRedirectUri = appTokenRedirectUri;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Long lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
