package mrkresnofatih.com.gitlab.authmanager.services.logincore;

public class LoginCoreGetTokenResponse {
    private String accessToken;

    public LoginCoreGetTokenResponse() {
    }

    public LoginCoreGetTokenResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
