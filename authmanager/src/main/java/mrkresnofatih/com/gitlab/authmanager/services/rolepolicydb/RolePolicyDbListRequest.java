package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;


import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

public class RolePolicyDbListRequest {
    @NotNull
    private String roleName;

    @NotNull
    private String rolePolicyName;

    @NotNull
    private String resourceSchemaItemName;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private Integer page;

    @NotNull
    @Range(min = 1, max = 25)
    private Integer pageSize;

    public RolePolicyDbListRequest() {
    }

    public RolePolicyDbListRequest(String roleName, String rolePolicyName, String resourceSchemaItemName, Integer page, Integer pageSize) {
        this.roleName = roleName;
        this.rolePolicyName = rolePolicyName;
        this.resourceSchemaItemName = resourceSchemaItemName;
        this.page = page;
        this.pageSize = pageSize;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRolePolicyName() {
        return rolePolicyName;
    }

    public void setRolePolicyName(String rolePolicyName) {
        this.rolePolicyName = rolePolicyName;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
