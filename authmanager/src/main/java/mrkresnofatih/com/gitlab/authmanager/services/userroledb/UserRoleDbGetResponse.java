package mrkresnofatih.com.gitlab.authmanager.services.userroledb;


public class UserRoleDbGetResponse {
    private String roleName;

    private String principalName;

    private String userRoleId;

    private Boolean active;

    public UserRoleDbGetResponse() {
    }

    public UserRoleDbGetResponse(String roleName, String principalName, String userRoleId, Boolean active) {
        this.roleName = roleName;
        this.principalName = principalName;
        this.userRoleId = userRoleId;
        this.active = active;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
