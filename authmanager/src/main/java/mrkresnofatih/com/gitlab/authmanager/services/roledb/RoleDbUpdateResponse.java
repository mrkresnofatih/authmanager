package mrkresnofatih.com.gitlab.authmanager.services.roledb;

public class RoleDbUpdateResponse {
    private String message;

    public RoleDbUpdateResponse() {
    }

    public RoleDbUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
