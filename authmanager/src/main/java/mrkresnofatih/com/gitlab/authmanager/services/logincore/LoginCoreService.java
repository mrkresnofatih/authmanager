package mrkresnofatih.com.gitlab.authmanager.services.logincore;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface LoginCoreService {
    FuncResponse<LoginCoreGetTokenResponse> getToken(FuncRequest<LoginCoreGetTokenRequest> getTokenRequest);

    FuncResponse<LoginCoreGetProfileResponse> getProfile(FuncRequest<LoginCoreGetProfileRequest> getProfileRequest);
}
