package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class ResourceSchemaItemGetRequest {
    @NotNull
    @NotBlank
    private String resourceSchemaItemName;

    public ResourceSchemaItemGetRequest() {
    }

    public ResourceSchemaItemGetRequest(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }
}
