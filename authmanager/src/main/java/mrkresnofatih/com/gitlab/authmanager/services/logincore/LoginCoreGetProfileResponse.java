package mrkresnofatih.com.gitlab.authmanager.services.logincore;

public class LoginCoreGetProfileResponse {
    private String username;

    public LoginCoreGetProfileResponse() {
    }

    public LoginCoreGetProfileResponse(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
