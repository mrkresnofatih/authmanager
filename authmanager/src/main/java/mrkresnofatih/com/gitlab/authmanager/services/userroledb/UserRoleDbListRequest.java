package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

import jakarta.validation.constraints.NotNull;

public class UserRoleDbListRequest {
    @NotNull
    private String roleName;

    @NotNull
    private String principalName;

    @NotNull
    private Boolean active;

    @NotNull
    private Integer page;

    @NotNull
    private Integer pageSize;

    public UserRoleDbListRequest() {
    }

    public UserRoleDbListRequest(String roleName, String principalName, Boolean active, Integer page, Integer pageSize) {
        this.roleName = roleName;
        this.principalName = principalName;
        this.active = active;
        this.page = page;
        this.pageSize = pageSize;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
