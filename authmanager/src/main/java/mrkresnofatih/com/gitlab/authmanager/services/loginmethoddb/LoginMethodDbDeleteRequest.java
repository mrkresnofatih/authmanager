package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class LoginMethodDbDeleteRequest {
    @NotNull
    @NotBlank
    private String loginMethodId;

    public LoginMethodDbDeleteRequest() {
    }

    public LoginMethodDbDeleteRequest(String loginMethodId) {
        this.loginMethodId = loginMethodId;
    }

    public String getLoginMethodId() {
        return loginMethodId;
    }

    public void setLoginMethodId(String loginMethodId) {
        this.loginMethodId = loginMethodId;
    }
}
