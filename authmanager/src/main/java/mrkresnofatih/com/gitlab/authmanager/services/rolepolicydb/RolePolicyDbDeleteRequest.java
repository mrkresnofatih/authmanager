package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class RolePolicyDbDeleteRequest {
    @NotNull
    @NotBlank
    private String rolePolicyName;

    public RolePolicyDbDeleteRequest() {
    }

    public RolePolicyDbDeleteRequest(String rolePolicyName) {
        this.rolePolicyName = rolePolicyName;
    }

    public String getRolePolicyName() {
        return rolePolicyName;
    }

    public void setRolePolicyName(String rolePolicyName) {
        this.rolePolicyName = rolePolicyName;
    }
}
