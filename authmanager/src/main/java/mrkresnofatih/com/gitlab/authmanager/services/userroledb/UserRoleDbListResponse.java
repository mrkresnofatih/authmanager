package mrkresnofatih.com.gitlab.authmanager.services.userroledb;


import java.util.List;

public class UserRoleDbListResponse {
    private Integer page;

    private Integer pageSize;

    private List<UserRoleDbGetResponse> userRoles;

    public UserRoleDbListResponse() {
    }

    public UserRoleDbListResponse(Integer page, Integer pageSize, List<UserRoleDbGetResponse> userRoles) {
        this.page = page;
        this.pageSize = pageSize;
        this.userRoles = userRoles;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<UserRoleDbGetResponse> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoleDbGetResponse> userRoles) {
        this.userRoles = userRoles;
    }
}
