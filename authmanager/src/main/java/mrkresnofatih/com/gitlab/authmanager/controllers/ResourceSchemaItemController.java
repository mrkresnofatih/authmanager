package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem.*;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resource-schema-item")
public class ResourceSchemaItemController {
    private final ResourceSchemaItemService resourceSchemaItemService;

    @Autowired
    public ResourceSchemaItemController(ResourceSchemaItemService resourceSchemaItemService) {
        this.resourceSchemaItemService = resourceSchemaItemService;
    }

    @PostMapping("/create")
    public FuncResponse<ResourceSchemaItemCreateResponse> create(@Valid @RequestBody FuncRequest<ResourceSchemaItemCreateRequest> createRequest) {
        return resourceSchemaItemService.create(createRequest);
    }

    @PostMapping("/get")
    public FuncResponse<ResourceSchemaItemGetResponse> get(@Valid @RequestBody FuncRequest<ResourceSchemaItemGetRequest> getRequest) {
        return resourceSchemaItemService.get(getRequest);
    }

    @PostMapping("/list")
    public FuncResponse<ResourceSchemaItemListResponse> list(@Valid @RequestBody FuncRequest<ResourceSchemaItemListRequest> listRequest) {
        return resourceSchemaItemService.list(listRequest);
    }

    @PostMapping("/update")
    public FuncResponse<ResourceSchemaItemUpdateResponse> update(@Valid @RequestBody FuncRequest<ResourceSchemaItemUpdateRequest> updateRequest) {
        return resourceSchemaItemService.update(updateRequest);
    }

    @PostMapping("/delete")
    public FuncResponse<ResourceSchemaItemDeleteResponse> delete(@Valid @RequestBody FuncRequest<ResourceSchemaItemDeleteRequest> deleteRequest) {
        return resourceSchemaItemService.delete(deleteRequest);
    }
}
