package mrkresnofatih.com.gitlab.authmanager.services.userdb;

import java.util.List;

public class UserDbListResponse {
    private Integer page;
    private Integer pageSize;
    private List<UserDbGetResponse> users;

    public UserDbListResponse() {
    }

    public UserDbListResponse(Integer page, Integer pageSize, List<UserDbGetResponse> users) {
        this.page = page;
        this.pageSize = pageSize;
        this.users = users;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<UserDbGetResponse> getUsers() {
        return users;
    }

    public void setUsers(List<UserDbGetResponse> users) {
        this.users = users;
    }
}
