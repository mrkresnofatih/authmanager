package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public class ResourceSchemaItemUpdateRequest {
    @NotNull
    @NotBlank
    private String resourceSchemaItemName;

    @NotNull
    @NotBlank
    private String parentResourceSchemaItemName;

    @NotNull
    @NotEmpty
    private List<String> availableActions;

    public ResourceSchemaItemUpdateRequest() {
    }

    public ResourceSchemaItemUpdateRequest(String resourceSchemaItemName, String parentResourceSchemaItemName, List<String> availableActions) {
        this.resourceSchemaItemName = resourceSchemaItemName;
        this.parentResourceSchemaItemName = parentResourceSchemaItemName;
        this.availableActions = availableActions;
    }

    public String getResourceSchemaItemName() {
        return resourceSchemaItemName;
    }

    public void setResourceSchemaItemName(String resourceSchemaItemName) {
        this.resourceSchemaItemName = resourceSchemaItemName;
    }

    public String getParentResourceSchemaItemName() {
        return parentResourceSchemaItemName;
    }

    public void setParentResourceSchemaItemName(String parentResourceSchemaItemName) {
        this.parentResourceSchemaItemName = parentResourceSchemaItemName;
    }

    public List<String> getAvailableActions() {
        return availableActions;
    }

    public void setAvailableActions(List<String> availableActions) {
        this.availableActions = availableActions;
    }
}
