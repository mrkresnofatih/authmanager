package mrkresnofatih.com.gitlab.authmanager.services.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
public class Auth0JwtServiceImpl implements JwtService {
    private final Logger logger;

    @Autowired
    public Auth0JwtServiceImpl() {
        this.logger = LoggerFactory.getLogger(Auth0JwtServiceImpl.class);
    }

    @Override
    public FuncResponse<JwtCreateResponse> create(FuncRequest<JwtCreateRequest> createRequest) {
        logger.info("creating JWT Token w. data: {}", createRequest);
        try {
            var rsaKey = getRsaKeys();
            // Generate private and public key objects
            RSAPrivateKey privateKeyImpl = (RSAPrivateKey) rsaKey.get("private");
            RSAPublicKey publicKeyImpl = (RSAPublicKey) rsaKey.get("public");
            Algorithm algorithm = Algorithm.RSA256(publicKeyImpl, privateKeyImpl);
            String token = JWT.create()
                    .withIssuer("https://app-dev.authmanager.com")
                    .withExpiresAt(Instant.now().plusSeconds(createRequest.getData().getDuration()))
                    .withClaim("data", createRequest.getData().getClaims())
                    .sign(algorithm);
            return new FuncResponse<>(new JwtCreateResponse(token));
        } catch (Exception exception){
            logger.error("failed to create jwt token, cause: {} & corr-id: {}", exception.getMessage(), createRequest.getCorrelationId());
            return new FuncResponse<>("failed to create jwt");
        }
    }

    private Map<String, Object> getRsaKeys() {
        try {
            // Load private key
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(getPrivateKey());
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            // Load public key
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(getPublicKey());
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            Map<String, Object> keys = new HashMap<>();
            keys.put("private", privateKey);
            keys.put("public", publicKey);
            return keys;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] getPrivateKey() {
        return Base64.getDecoder().decode("MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDsKPTaZZjVLPJzRgjNvrF6KHcF4+w9bj+06oPPW0LdLxzIvXP4hWj4kcbsEW/k6FH/pWarbLIJDT6EbZdsY/lwjYXGDVsbw6jCLybHZdtCKfG053Cc98bT9K+/A95nx6SzdzNW+LRQIQov31PwbMT/zcM/nCBMQMH01AMZ4D8rnWHOE7XvFfomqdNIZe9gsVKifng3tMxZTRdUyMllzPbyhH1k94optAr/DI8aGZc63ESimO7A+CL0RDI8hBOzStSGqw4/rL8pWTNwtspM66xiP+19MHO34y78SvvlpLX79wcZMDT/BCuK8faBJRxT/S5eeX0DS6cg91o90rMomXHLAgMBAAECggEAIDfyN712nVa1eQ6L8u3aLq1Il7AhsbvRyy/XbbmopJ5h0e0u3i/tQ0Q4HErAtj0Pus3o9OguXdfFuLFu4CW/iSB1qLB3xpDp/BwMsVVvFVa9mA2fKnsPUClotKmldQkiqi1P/sSNbQUk66UPQZTFR5Td11bIfT60d19ZUPVsCNn7vhPHyjr4W33+7bqz8TovtjsBBH/C4CipiJwnI4Kcai6m3eCdMwsGJqcscQed7T/IATK9fCzJtFMrE/dBFXgsmwlWHAWoGl4G0tfvSdEAhXIUtsm/54Vltcft3FrZMJUot7xoJgXkrdRcT+KggiVxQu44pisHPGWVUCru1CnP+QKBgQD5a6j/2WkKUjmlPzpJhn9RPC99QvJYOfTfnqDeg1F1XWP9p74suH6XeKkoRRlFS0RVJQL6waYpOPCAzq71a59IdqZ3HePT4AqC4c7l6q4xikCEA1tNYV3hcWd3vRi/a0Bmg2ZNtjVcn02KUep/Ok22TJ+L1soVhRez1lKTOsSYHwKBgQDyY79hXKIC22K9Qv1F78fA2dGX0TRtbMAh6ikvPUDl+QXIpmtMGdiBemHS/5qtXLgNsrSn5UHUl7ZRvI+2aROQx+UtPXM/hX0KJuRlLaJ0rM2du7sEdVlROyFB5T2q0igPKlt4+ygBhXtNgy4eaNrOcTYQUIGHUVluStx9+50g1QKBgBEE/6s1sq05nyVfgyKxQs7QRI1PnCnZCpSXxnJok45mqQQOBjQXAsxjN2fAD1FjKW6F8AYcYLI/aGnhsJ6ekWfocMOADqPOjekQb4XQ1cIbpPtdH0XQdw+DJYROKvok32gfX0O+QzXmyOu/WzkDSXo/EEQCjBeb1Vo4LbpOMEwRAoGANggYNEri7oIbC5dwP/PIcmvbtdzzs+THG4hIL/iWi+0r5eN7DZbBVXODdPZ3c2e0u5D3hmmreAltoeDHpGDd9KsqoiANnb5S9j1fMlKu4RVYLG4ZrU+/XkUxD7BbQ8XfGipWA6zw6Q3pQ4kXcb3r8JL5+z8G8LQonFZhuk+pYhUCgYB4o4dJjeCrowcaPB3a4i4CcKCcrDrTIglOooL6IxTytZhny3Ue5TZz3FB26iBD1Ftw/jSfPwnqTAlS6Ml2tVbZUwbwSvPULPCViIsAa5SnhxaYXZRD4nKr8nDk+kLPxJ/u8Ujo4Z67tlX0sFMuiF72HKq0Ecj/PEBIsoxUt/Ndlg==");
    }

    private byte[] getPublicKey() {
        return Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7Cj02mWY1Szyc0YIzb6xeih3BePsPW4/tOqDz1tC3S8cyL1z+IVo+JHG7BFv5OhR/6Vmq2yyCQ0+hG2XbGP5cI2Fxg1bG8Oowi8mx2XbQinxtOdwnPfG0/SvvwPeZ8eks3czVvi0UCEKL99T8GzE/83DP5wgTEDB9NQDGeA/K51hzhO17xX6JqnTSGXvYLFSon54N7TMWU0XVMjJZcz28oR9ZPeKKbQK/wyPGhmXOtxEopjuwPgi9EQyPIQTs0rUhqsOP6y/KVkzcLbKTOusYj/tfTBzt+Mu/Er75aS1+/cHGTA0/wQrivH2gSUcU/0uXnl9A0unIPdaPdKzKJlxywIDAQAB");
    }

    private Map<String, Object> getAutoGeneratedRsaKeys() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();
            byte[] publicKeyBytes = publicKey.getEncoded();
            String publicKeyBase64 = Base64.getEncoder().encodeToString(publicKeyBytes);
            logger.info("pubkey: {}", publicKeyBase64);
            byte[] privateKeyBytes = privateKey.getEncoded();
            String privateKeyBase64 = Base64.getEncoder().encodeToString(privateKeyBytes);
            logger.info("prvkey: {}", privateKeyBase64);
            Map<String, Object> keys = new HashMap<String, Object>();
            keys.put("private", privateKey);
            keys.put("public", publicKey);
            return keys;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
