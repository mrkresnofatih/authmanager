package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;

public interface LoginMethodDbService {
    FuncResponse<LoginMethodDbCreateResponse> create(FuncRequest<LoginMethodDbCreateRequest> createRequest);
    FuncResponse<LoginMethodDbGetResponse> get(FuncRequest<LoginMethodDbGetRequest> getRequest);
    FuncResponse<LoginMethodDbListResponse> list(FuncRequest<LoginMethodDbListRequest> listRequest);
    FuncResponse<LoginMethodDbUpdateResponse> update(FuncRequest<LoginMethodDbUpdateRequest> updateRequest);
    FuncResponse<LoginMethodDbDeleteResponse> delete(FuncRequest<LoginMethodDbDeleteRequest> deleteRequest);
}
