package mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb;

public class LoginMethodDbCreateResponse {
    private String message;

    public LoginMethodDbCreateResponse() {
    }

    public LoginMethodDbCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
