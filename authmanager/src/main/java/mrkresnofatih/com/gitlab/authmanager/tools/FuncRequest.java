package mrkresnofatih.com.gitlab.authmanager.tools;


import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public class FuncRequest<T> extends JsonSerializable {
    @NotNull
    @Valid
    private T data;

    @NotNull
    private String correlationId;

    public FuncRequest() {
    }

    public FuncRequest(T data, String correlationId) {
        this.data = data;
        this.correlationId = correlationId;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCorrelationId() {
        if (correlationId.isEmpty()) {
            correlationId = UUID.randomUUID().toString();
        }
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        return super.toJson();
    }
}
