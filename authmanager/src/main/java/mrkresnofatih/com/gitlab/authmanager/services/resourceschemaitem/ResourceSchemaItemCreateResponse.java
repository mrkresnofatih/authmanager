package mrkresnofatih.com.gitlab.authmanager.services.resourceschemaitem;

public class ResourceSchemaItemCreateResponse {
    private String message;

    public ResourceSchemaItemCreateResponse() {
    }

    public ResourceSchemaItemCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
