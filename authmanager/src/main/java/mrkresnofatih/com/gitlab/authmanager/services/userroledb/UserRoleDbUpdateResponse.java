package mrkresnofatih.com.gitlab.authmanager.services.userroledb;

public class UserRoleDbUpdateResponse {
    private String message;

    public UserRoleDbUpdateResponse() {
    }

    public UserRoleDbUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
