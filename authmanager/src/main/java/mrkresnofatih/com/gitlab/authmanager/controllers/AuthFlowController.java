package mrkresnofatih.com.gitlab.authmanager.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.authmanager.services.authflow.AuthFlowService;
import mrkresnofatih.com.gitlab.authmanager.services.authflow.AuthFlowSyncLoginRequest;
import mrkresnofatih.com.gitlab.authmanager.services.authflow.AuthFlowSyncLoginResponse;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth-flow")
public class AuthFlowController {
    private final AuthFlowService authFlowService;

    @Autowired
    public AuthFlowController(AuthFlowService authFlowService) {
        this.authFlowService = authFlowService;
    }

    @PostMapping("/sync-login")
    public FuncResponse<AuthFlowSyncLoginResponse> syncLogin(@Valid @RequestBody FuncRequest<AuthFlowSyncLoginRequest> syncLoginRequest) {
        return authFlowService.syncLogin(syncLoginRequest);
    }
}
