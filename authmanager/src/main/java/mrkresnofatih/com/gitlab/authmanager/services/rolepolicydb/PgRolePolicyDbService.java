package mrkresnofatih.com.gitlab.authmanager.services.rolepolicydb;

import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationConfig;
import mrkresnofatih.com.gitlab.authmanager.configurations.PostgresDbMigrationRunner;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PgRolePolicyDbService implements RolePolicyDbService {
    private final Connection postgresConnection;
    private final Logger logger;

    @Autowired
    public PgRolePolicyDbService(PostgresDbMigrationConfig postgresDbMigrationConfig, Connection postgresConnection) {
        PostgresDbMigrationRunner.runDbMigration(postgresConnection, postgresDbMigrationConfig);
        this.postgresConnection = postgresConnection;
        this.logger = LoggerFactory.getLogger(PgRolePolicyDbService.class);
    }

    @Override
    public FuncResponse<RolePolicyDbCreateResponse> create(FuncRequest<RolePolicyDbCreateRequest> createRequest) {
        logger.info("start create role_policy w. data: {}", createRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO role_policies (role_policy_name, role_name, granted_actions, granted_resources, resource_schema_item_name, effect) " +
                    "VALUES ('%s', '%s', %s, %s, '%s', '%s');",
                    createRequest.getData().getRolePolicyName(),
                    createRequest.getData().getRoleName(),
                    psqlArrayStringBuilder(createRequest.getData().getGrantedActions()),
                    psqlArrayStringBuilder(createRequest.getData().getGrantedResources()),
                    createRequest.getData().getResourceSchemaItemName(),
                    createRequest.getData().getEffect()));
            statement.close();
            return new FuncResponse<>(new RolePolicyDbCreateResponse("role_policy created"));
        } catch (SQLException e) {
            logger.error("failed to create role_policy, corr-id: {} & cause: {}", createRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed create role_policy");
        }
    }

    private String psqlArrayStringBuilder(List<String> list) {
        var stringBuilder = new StringBuilder("ARRAY[");
        var iterator = list.iterator();
        while (iterator.hasNext()) {
            var string = iterator.next();
            if (iterator.hasNext()) {
                stringBuilder.append(String.format("'%s', ", string));
            }
            else {
                stringBuilder.append(String.format("'%s'", string));
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public FuncResponse<RolePolicyDbGetResponse> get(FuncRequest<RolePolicyDbGetRequest> getRequest) {
        logger.info("start get role_policy w. data: {}", getRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var result = statement.executeQuery(String.format("SELECT * FROM role_policies WHERE role_policy_name = '%s' LIMIT 1;",
                    getRequest.getData().getRolePolicyName()));
            if (result.next()) {
                var rolePolicyName = result.getObject("role_policy_name", String.class);
                var roleName = result.getObject("role_name", String.class);
                var grantedActions = Arrays.asList((String[]) result.getArray("granted_actions").getArray());
                var grantedResources = Arrays.asList((String[]) result.getArray("granted_resources").getArray());
                var resourceSchemaItemName = result.getObject("resource_schema_item_name", String.class);
                var effect = result.getObject("effect", String.class);
                return new FuncResponse<>(new RolePolicyDbGetResponse(
                        roleName,
                        rolePolicyName,
                        grantedActions,
                        grantedResources,
                        resourceSchemaItemName,
                        effect
                ));
            }
            statement.close();
            return new FuncResponse<>("role_policy not found");
        } catch (SQLException e) {
            logger.error("failed to get role_policy, corr-id: {} & cause: {}", getRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to get role_policy");
        }
    }

    @Override
    public FuncResponse<RolePolicyDbUpdateResponse> update(FuncRequest<RolePolicyDbUpdateRequest> updateRequest) {
        logger.info("start update role_policy w. data: {}", updateRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("UPDATE role_policies SET " +
                    "role_name = '%s', " +
                    "granted_actions = %s, " +
                    "granted_resources = %s, effect = '%s', " +
                    "resource_schema_item_name = '%s' WHERE role_policy_name = '%s';",
                    updateRequest.getData().getRoleName(),
                    psqlArrayStringBuilder(updateRequest.getData().getGrantedActions()),
                    psqlArrayStringBuilder(updateRequest.getData().getGrantedResources()),
                    updateRequest.getData().getEffect(),
                    updateRequest.getData().getResourceSchemaItemName(),
                    updateRequest.getData().getRolePolicyName()));
            statement.close();
            return new FuncResponse<>(new RolePolicyDbUpdateResponse("role_policy updated"));
        } catch (SQLException e) {
            logger.error("failed to update role_policy, corr-id: {} & cause: {}", updateRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to update role_policy");
        }
    }

    @Override
    public FuncResponse<RolePolicyDbDeleteResponse> delete(FuncRequest<RolePolicyDbDeleteRequest> deleteRequest) {
        logger.info("start delete role_policy w. data: {}", deleteRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            statement.executeUpdate(String.format("DELETE FROM role_policies WHERE role_policy_name = '%s';",
                    deleteRequest.getData().getRolePolicyName()));
            statement.close();
            return new FuncResponse<>(new RolePolicyDbDeleteResponse("role_policy deleted"));
        } catch (SQLException e) {
            logger.error("failed to delete role_policy, corr-id: {} & cause: {}", deleteRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to delete role_policy");
        }
    }

    @Override
    public FuncResponse<RolePolicyDbListResponse> list(FuncRequest<RolePolicyDbListRequest> listRequest) {
        logger.info("start list role_policy w. data: {}", listRequest);
        try {
            Statement statement = postgresConnection.createStatement();
            var sql = getListSqlQuery(listRequest);
            var result = statement.executeQuery(sql);
            var rolePolicies = new ArrayList<RolePolicyDbGetResponse>();
            while (result.next()) {
                var rolePolicyName = result.getObject("role_policy_name", String.class);
                var roleName = result.getObject("role_name", String.class);
                var grantedActions = Arrays.asList((String[]) result.getArray("granted_actions").getArray());
                var grantedResources = Arrays.asList((String[]) result.getArray("granted_resources").getArray());
                var resourceSchemaItemName = result.getObject("resource_schema_item_name", String.class);
                var effect = result.getObject("effect", String.class);
                rolePolicies.add(new RolePolicyDbGetResponse(
                        roleName,
                        rolePolicyName,
                        grantedActions,
                        grantedResources,
                        resourceSchemaItemName,
                        effect
                ));
            }
            return new FuncResponse<>(new RolePolicyDbListResponse(
                    listRequest.getData().getPage(),
                    listRequest.getData().getPageSize(),
                    rolePolicies
            ));
        } catch (SQLException e) {
            logger.error("failed to list role_policies, corr-id: {} & cause: {}", listRequest.getCorrelationId(), e.getMessage());
            return new FuncResponse<>("failed to list role_policies");
        }
    }

    private String getListSqlQuery(FuncRequest<RolePolicyDbListRequest> listRequest) {
        var sqlBuilder = new StringBuilder("SELECT * FROM role_policies WHERE ");
        var stringList = getQualifiers(listRequest);
        for (int i = 1; i <= stringList.size(); i++) {
            sqlBuilder.append(stringList.get(i - 1));
            if (i != stringList.size()) {
                sqlBuilder.append("AND ");
            }
        }
        sqlBuilder.append(String.format("LIMIT %d OFFSET %d;",
                listRequest.getData().getPageSize(),
                listRequest.getData().getPageSize() * (listRequest.getData().getPage() - 1)));
        return sqlBuilder.toString();
    }

    private static ArrayList<Object> getQualifiers(FuncRequest<RolePolicyDbListRequest> listRequest) {
        var stringList = new ArrayList<>();
        if (!listRequest.getData().getRoleName().isBlank()) {
            stringList.add(String.format("role_name = '%s' ", listRequest.getData().getRoleName()));
        }
        if (!listRequest.getData().getRolePolicyName().isBlank()) {
            stringList.add(String.format("role_policy_name = '%s' ", listRequest.getData().getRolePolicyName()));
        }
        if (!listRequest.getData().getResourceSchemaItemName().isBlank()) {
            stringList.add(String.format("resource_schema_item_name = '%s' ", listRequest.getData().getResourceSchemaItemName()));
        }
        return stringList;
    }
}
