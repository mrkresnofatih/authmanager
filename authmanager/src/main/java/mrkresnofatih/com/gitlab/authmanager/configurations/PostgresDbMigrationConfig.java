package mrkresnofatih.com.gitlab.authmanager.configurations;

public class PostgresDbMigrationConfig {
    private Boolean isExecuted;

    public PostgresDbMigrationConfig() {
        isExecuted = false;
    }

    public Boolean getExecuted() {
        return isExecuted;
    }

    public void setExecuted(Boolean executed) {
        isExecuted = executed;
    }
}
