package mrkresnofatih.com.gitlab.authmanager.services.authflow;

import mrkresnofatih.com.gitlab.authmanager.services.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.authmanager.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.authmanager.services.logincore.*;
import mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb.LoginMethodDbGetRequest;
import mrkresnofatih.com.gitlab.authmanager.services.loginmethoddb.LoginMethodDbService;
import mrkresnofatih.com.gitlab.authmanager.services.userdb.UserDbCreateRequest;
import mrkresnofatih.com.gitlab.authmanager.services.userdb.UserDbGetRequest;
import mrkresnofatih.com.gitlab.authmanager.services.userdb.UserDbService;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncRequest;
import mrkresnofatih.com.gitlab.authmanager.tools.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SimpleAuthFlowServiceImpl implements AuthFlowService {
    private final Logger logger;
    private final LoginMethodDbService loginMethodDbService;
    private final UserDbService userDbService;
    private final JwtService jwtService;

    @Autowired
    public SimpleAuthFlowServiceImpl(
            LoginMethodDbService loginMethodDbService,
            UserDbService userDbService, JwtService jwtService) {
        this.loginMethodDbService = loginMethodDbService;
        this.userDbService = userDbService;
        this.jwtService = jwtService;
        this.logger = LoggerFactory.getLogger(SimpleAuthFlowServiceImpl.class);
    }

    @Override
    public FuncResponse<AuthFlowSyncLoginResponse> syncLogin(FuncRequest<AuthFlowSyncLoginRequest> syncLoginRequest) {
        logger.info("start sync login w. data: {}", syncLoginRequest);
        var loginMethodResponse = loginMethodDbService
                .get(new FuncRequest<>(
                        new LoginMethodDbGetRequest(syncLoginRequest.getData().getLoginMethodId()),
                        syncLoginRequest.getCorrelationId()));
        if (loginMethodResponse.isError()) {
            return new FuncResponse<>(loginMethodResponse.getErrorMessage());
        }
        var loginMethodData = loginMethodResponse.getData();
        var callbackUrl = String.format("http://localhost:9000/?login-method-id=%s", loginMethodData.getLoginMethodId());
        LoginCoreService loginCoreServiceImplementation;
        switch (loginMethodData.getLoginMethodType()) {
            case "social:spotify": {
                loginCoreServiceImplementation = new SpotifyLoginCoreServiceImpl();
                break;
            }
            case "social:discord": {
                loginCoreServiceImplementation = new DiscordLoginCoreServiceImpl();
                break;
            }
            case "social:github": {
                loginCoreServiceImplementation = new GithubLoginCoreServiceImpl();
                break;
            }
            default: {
                return new FuncResponse<>("no implementation found");
            }
        }

        var getTokenResponse = loginCoreServiceImplementation
                .getToken(new FuncRequest<>(new LoginCoreGetTokenRequest(
                        syncLoginRequest.getData().getAuthorizationCode(),
                        loginMethodData.getClientId(),
                        loginMethodData.getClientSecret(),
                        callbackUrl),
                        syncLoginRequest.getCorrelationId()));
        if (getTokenResponse.isError()) {
            return new FuncResponse<>(getTokenResponse.getErrorMessage());
        }
        var token = getTokenResponse.getData().getAccessToken();
        var getProfileResponse = loginCoreServiceImplementation
                .getProfile(new FuncRequest<>(new LoginCoreGetProfileRequest(token), syncLoginRequest.getCorrelationId()));
        if (getProfileResponse.isError()) {
            return new FuncResponse<>(getProfileResponse.getErrorMessage());
        }
        var principalName = String.format("%s:%s",
                loginMethodData.getLoginMethodId(),
                getProfileResponse.getData().getUsername());
        var getUserResponse = userDbService
                .get(new FuncRequest<>(new UserDbGetRequest(principalName), syncLoginRequest.getCorrelationId()));
        if (getUserResponse.isError()) {
            logger.info("user w. principal_name {} not found, will attempt to create new user, corr-id: {}", principalName, syncLoginRequest.getCorrelationId());
            var createUserResponse = userDbService
                    .create(new FuncRequest<>(new UserDbCreateRequest(
                            UUID.randomUUID().toString(),
                            principalName,
                            System.currentTimeMillis(),
                            System.currentTimeMillis(),
                            true
                    ), syncLoginRequest.getCorrelationId()));
            if (createUserResponse.isError()) {
                return new FuncResponse<>(createUserResponse.getErrorMessage());
            }
        }
        var jwtCreateResponse = jwtService.create(new FuncRequest<>(new JwtCreateRequest(
                86400L,
                "claims"
        ), syncLoginRequest.getCorrelationId()));
        if (jwtCreateResponse.isError()) {
            return new FuncResponse<>(jwtCreateResponse.getErrorMessage());
        }
        return new FuncResponse<>(new AuthFlowSyncLoginResponse(
                jwtCreateResponse.getData().getJwtToken(),
                loginMethodData.getAppTokenRedirectUri()
        ));
    }
}
