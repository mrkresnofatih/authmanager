package mrkresnofatih.com.gitlab.authmanager.services.authflow;

public class AuthFlowSyncLoginResponse {
    private String appToken;
    private String appRedirectUri;

    public AuthFlowSyncLoginResponse() {
    }

    public AuthFlowSyncLoginResponse(String appToken, String appRedirectUri) {
        this.appToken = appToken;
        this.appRedirectUri = appRedirectUri;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public String getAppRedirectUri() {
        return appRedirectUri;
    }

    public void setAppRedirectUri(String appRedirectUri) {
        this.appRedirectUri = appRedirectUri;
    }
}
