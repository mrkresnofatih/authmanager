# AuthManager

A User Authentication Microservice featuring multi-userpools, roles, & permissions management implemented using Java, SpringBoot, PostgreSQL.
